from selenium import webdriver
import unittest
import os, os.path 

class NewVisitorTest(unittest.TestCase):
    dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    chromedriver_path = '/selenium_driver/chromedriver'
    chromedriver = dir_path + chromedriver_path 
    print('-----------------------')
    print(chromedriver)
    print('-----------------------')
    os.environ["webdriver.chrome.driver"] = chromedriver

    def setUp(self, chromedriver=chromedriver):
        self.browser = webdriver.Chrome(chromedriver)
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_it_worked(self):
        self.browser.get('http://localhost:8000')
        self.assertIn('Welcome to Django', self.browser.title)

if __name__ == '__main__':
    unittest.main()

